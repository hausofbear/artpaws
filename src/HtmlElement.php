<?php
    namespace Bearlovescode\Artpaws;


    class HtmlElement
    {

        public static function checkboxGroup($options = array())
        {
            $el = '<fieldset';

            if (isset($options['id']) && !empty($options['id']))
            {
                $el .= ' id="' . htmlentities($options['id']) . '"';
            }

            $el .= '>';

            if (is_array($options['values']))
            {
                foreach ($options['values'] as $box)
                {
                    $el .= self::checkbox($box);
                }
            }

            $el .= '</fieldset>';

            return $el;
        }

        /**
         * Create a single checkbox element.
         *
         * @author Nic "4tb" Barr <hausofbear@gmail.com>
         * @since 1.0-ALPHA
         * @param $options
         * @return string
         */
        public static function checkbox($options = array())
        {
            $el = '<div><input type="checkbox"';

            if (isset($options['id']) && !empty($options['id']))
            {
                $el .= ' id="' . htmlentities($options['id']) . '"';
            }

            if (isset($options['name']) && !empty($options['name']))
            {
                $el .= ' name="' . htmlentities($options['name']) . '"';
            }

            if (isset($options['checked']) && !empty($options['checked']))
            {
                $el .= ' checked="checked"';
            }

            $el .= ' value="' . htmlentities($options['value']) . '"';

            $el .= '> <span>' . $options['boxLabel'] . '</span></div>';

            return $el;
        }

        /**
         * create a <select> element for use in wordpress screens.
         *
         * @author Nic "4tb" Barr <hausofbear@gmail.com>
         * @since 1.0-ALPHA
         * @param array $options
         * @return string
         */
        public static function select($options = array())
        {
            $el = '<select';

            if (isset($options['id']) && !empty($options['id']))
            {
                $el .= ' id="' . htmlentities($options['id']) . '"';
            }

            if (isset($options['name']) && !empty($options['name']))
            {
                $el .= ' name="' . htmlentities($options['name']) . '"';
            }

            $el .= ">";

            foreach ($options['values'] as $k => $v)
            {
                $el .= '<option value="' . $k . '"';

                if (isset($options['selected']) && !is_null($options['selected']))
                {
                    $el .= ' selected="selected"';
                }

                $el .= '>' . $v . "</option>";
            }

            $el .= '</select>';

            return $el;
        }
    }