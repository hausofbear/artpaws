<?php
    namespace Bearlovescode\Artpaws;

    use Bearlovescode\Artpaws\HtmlElement;

    class Wordpress
    {
        protected $ap;

        /**
         * function called on register_activation_hook()
         *
         * @author Nic "4tb" Barr <hausofbear@gmail.com>
         * @since 1.0-ALPHA
         * @throws ArtpawsException
         */
        public static function activatePlugin()
        {
            $me = new Wordpress();
            $me->createSitesTable();
            $me->createCredentialsTable();
            unset($me);

        }

        /**
         * function called on register_deactivation_hook()
         *
         * @author Nic "4tb" Barr <hausofbear@gmail.com>
         * @since 1.0-ALPHA
         * @throws ArtpawsException
         */
        public static function deactivatePlugin()
        {
            $me = new Wordpress();
            $me->dropCredentialsTable();
            $me->dropSitesTable();
            unset($me);
        }

        public static function addMediaUploaderFields($form_fields, $post)
        {

            $value = get_post_meta($post->ID, '_ap-sub-type', true);
            $apSubTypeOpts = array(
                'id' => 'attachment-' . $post->ID . '-ap-sub-type',
                'name' => 'attachment[' . $post->ID . '][ap-sub-type]',
                'values' => array(
                    'artwork' => 'Artwork'
                ),
                'selected' => (isset($value)) ? $value : null
            );
            $form_fields['ap-sub-type']['label'] = __('Submission Type');
            $form_fields['ap-sub-type']['input'] = 'html';
            $form_fields['ap-sub-type']['html'] = HtmlElement::select($apSubTypeOpts);
            $form_fields['ap-sub-type']['helps'] = __('Sets the submission type for the destination art site.');
            $form_fields['ap-sub-type']['required'] = true;

            $apSubRatingOptions = array(
                'id' => 'attachments-' . $post->ID . '-ap-sub-rating',
                'name' => 'attachments[' . $post->ID . '][ap-sub-rating]',
                'values' => array(
                    'general' => 'General',
                    'mature' => 'Mature',
                    'adult' => 'Adult'
                )
            );
            $form_fields['ap-sub-rating']['label'] = __('Submission Rating');
            $form_fields['ap-sub-rating']['input'] = 'html';
            $form_fields['ap-sub-rating']['html'] = HtmlElement::select($apSubRatingOptions);
            $form_fields['ap-sub-rating']['helps'] = __('Sets the submission rating for the destination art site.');
            $form_fields['ap-sub-rating']['required'] = true;
            error_log(HtmlElement::select($apSubRatingOptions));

            $value = get_post_meta($post->ID, '_ap-sub-folder', true);
            $form_fields['ap-sub-folder']['label'] = __('Submission Folder');
            $form_fields['ap-sub-folder']['type'] = 'text';
            $form_fields['ap-sub-folder']['value'] = (isset($value)) ? $value : '';
            $form_fields['ap-sub-folder']['helps'] = 'Folder name to post this image to (if site supports this feature)';


            $apSubUploadSites = array(
                'id' => 'opt-ap-sub-upload-sites',
                'values' => array(
                    'fa' => array(
                        'id' => 'attachment-' . $post->ID . '-ap-sub-upload-site-fa',
                        'name' => 'attachment[' .$post->ID . '][ap-sub-upload-sites][]',
                        'boxLabel' => 'FurAffinity',
                        'value' => 'furaffinity'
                    ),
                    'weasyl' => array(
                        'id' => 'attachment-' . $post->ID . '-ap-sub-upload-site-weasyl',
                        'name' => 'attachment[' .$post->ID . '][ap-sub-upload-sites][]',
                        'boxLabel' => 'Weasyl',
                        'value' => 'weasyl'
                    ),
                    'fn' => array(
                        'id' => 'attachment-' . $post->ID . '-ap-sub-upload-site-fn',
                        'name' => 'attachment[' .$post->ID . '][ap-sub-upload-sites][]',
                        'boxLabel' => 'FurryNetwork',
                        'value' => 'furrynetwork'
                    ),
                    'ib' => array(
                        'id' => 'attachment-' . $post->ID . '-ap-sub-upload-site-ib',
                        'name' => 'attachment[' .$post->ID . '][ap-sub-upload-sites][]',
                        'boxLabel' => 'InkBunny',
                        'value' => 'inkbunny'
                    ),
                    'so' => array(
                        'id' => 'attachment-' . $post->ID . '-ap-sub-upload-site-so',
                        'name' => 'attachment[' .$post->ID . '][ap-sub-upload-sites][]',
                        'boxLabel' => 'SoFurry',
                        'value' => 'sofurry'
                    ),
                    'da' => array(
                        'id' => 'attachment-' . $post->ID . '-ap-sub-upload-site-da',
                        'name' => 'attachment[' .$post->ID . '][ap-sub-upload-sites][]',
                        'boxLabel' => 'DeviantArt',
                        'value' => 'deviantart'
                    )
                )
            );
            $form_fields['ap-sub-upload-sites']['label'] = __('Submission Sites');
            $form_fields['ap-sub-upload-sites']['input'] = 'html';
            $form_fields['ap-sub-upload-sites']['html'] = HtmlElement::checkboxGroup($apSubUploadSites);
            $form_fields['ap-sub-upload-sites']['helps'] = __('Choose the sites to upload this image to.');
            
            return $form_fields;
        }


        public static function saveMediaUploaderFields($post, $attachment)
        {
            $fields = array(
                'ap-sub-type',
                'ap-sub-rating',
                'ap-sub-folder',
                'ap-sub-upload-sites'
            );

            foreach ($fields as $fld)
            {
                update_post_meta($post['ID'], '_' . $fld, $attachment[$fld]);
            }

            return $post;
        }



        /**
         * create table with art-site-related information.
         * @author Nic "4tb" Barr <hausofbear@gmail.com>
         * @since 1.0-ALPHA
         * @throws ArtpawsException
         */
        public function createSitesTable()
        {
            global $wpdb;

            if ($wpdb)
            {
                $charsetCollate = $wpdb->get_charset_collate();
                $tableName = $wpdb->prefix . 'artpaws_sites';

                $sql = 'CREATE TABLE ' . $tableName . ' (
                    id mediumint(9) NOT NULL AUTO_INCREMENT,
                    name varchar(100) NOT NULL,
                    url varchar(255) NOT NULL,
                    is_active int(1) NOT NULL DEFAULT 1,
                    created datetime NOT NULL,
                    modified datetime NOT NULL,
                    UNIQUE KEY id (id)
                ) ' . $charsetCollate . ';';

                dbDelta($sql);

                $wpdb->insert(
                    $tableName,
                    array(
                        'name' => 'FurAffinity',
                        'url' => 'https://furaffinity.net/',
                        'is_active' => 1,
                        'created' => current_time('mysql'),
                        'modified' => current_time('mysql')
                    )
                );
            }

            else
            {
                throw new ArtpawsException('Could not get the $wpdb global variable');
            }

        }

        /**
         * drop table for artpaws sites.
         *
         * @author Nic "4tb" Barr <hausofbear@gmail.com>
         * @since 1.0-ALPHA
         */
        public function dropSitesTable()
        {
            global $wpdb;

            if ($wpdb)
            {
                $tableName = $wpdb->prefix . 'artpaws_sites';
                $wpdb->query('DROP TABLE IF EXISTS ' . $tableName);
            }

            else
            {
                throw new ArtpawsException('Could not load the $wpdb database.');
            }
        }

        /**
         * create table to hold data about sites that artpaws can post to.
         *
         * @author Nic "4tb" Barr <hausofbear@gmail.com>
         * @since 1.0-ALPHA
         * @throws ArtpawsException
         */
        public function createCredentialsTable()
        {
            global $wpdb;

            if ($wpdb)
            {
                $tableName = $wpdb->prefix . 'artpaws_credentials';
                $tableNameSites = $wpdb->prefix . 'artpaws_sites';
                $tableNameUsers = $wpdb->prefix . 'users';

                $charsetCollate = $wpdb->get_charset_collate();


                $sql = 'CREATE TABLE ' . $tableName . ' (
                    id mediumint(9) NOT NULL,
                    siteId mediumint(9) NOT NULL REFERENCES ' . $tableNameSites . '(id),
                    wpUserId mediumint(9) NOT NULL REFERENCES ' . $tableNameUsers . '(ID),
                    data text NOT NULL,
                    created datetime not null,
                    modified datetime not null
                ) ' . $charsetCollate . ';';
                dbDelta($sql);
            }

            else
            {
                throw new ArtpawsException('Could not load the $wpdb database.');
            }
        }

        /**
         * drop the artpaws_credentials table;
         *
         * @author Nic "4tb" Barr <hausofbear@gmail.com>
         * @since 1.0-ALPHA
         * @throws ArtpawsException
         */
        public function dropCredentialsTable()
        {
            global $wpdb;

            if ($wpdb)
            {
                $tableName = $wpdb->prefix . 'artpaws_credentials';
                $wpdb->query('DROP TABLE IF EXISTS ' . $tableName);
            }

            else
            {
                throw new ArtpawsException('Could not load the $wpdb database.');
            }
        }



        /**
         * Wordpress constructor.
         */
        public function __construct()
        {
            if (class_exists('Artpaws'))
            {
               $this->ap = new Artpaws();
            }

            // make sure the dbDelta() function exists.
            if (!function_exists('dbDelta'))
            {
                require_once ABSPATH.'wp-admin/includes/upgrade.php';
            }
        }
    }