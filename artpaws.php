<?php
    /*
     * Plugin Name: Artpaws
     * Plugin URI: http://artpaws.xyz
     * Description: Keep yer paws on your own artwork.
     * Version: 1.0-ALPHA
     * Author: Nic "4tb" Barr <hausofbear@gmail.com>
     * Author URI: http://bearlovescode.com/
     */


    // idea from https://gilbert.pellegrom.me/using-psr-4-autoloading-in-wordpress-plugins/
    spl_autoload_register(function($class) {
            $prefix = 'Bearlovescode\Artpaws';

            $baseDir = __DIR__ . '/src';

            $len = strlen($prefix);

            if (strncmp($prefix, $class, $len) !== 0)
            {
                return;
            }

            $relClass = substr($class, $len);
            $file = $baseDir . '/' . str_replace('\\', '/', $relClass) . '.php';
            if (file_exists($file))
            {
                require_once $file;
            }
        }
     );

    $apwp = new \Bearlovescode\Artpaws\Wordpress();

    if (isset($apwp))
    {
        register_activation_hook(__FILE__, array('\Bearlovescode\Artpaws\Wordpress', 'activatePlugin'));
        register_deactivation_hook(__FILE__, array('\Bearlovescode\Artpaws\Wordpress', 'deactivatePlugin'));
        // Actions.

        // Hooks.
        add_filter('attachment_fields_to_edit', array('\Bearlovescode\Artpaws\Wordpress', 'addMediaUploaderFields'), 10, 2);
        add_filter('attachment_fields_to_save', array('\Bearlovescode\Artpaws\Wordpress', 'saveMediaUploaderFields'), 10, 2);
    }

    else
    {
        throw new \Bearlovescode\Artpaws\ArtpawsException('Did not load the Wordpress class correctly.');
    }